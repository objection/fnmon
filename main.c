#define _GNU_SOURCE
#include "lib/slurp/slurp.h"
#include "lib/darr/darr.h"
#include "lib/time_string/time_string.h"
#include <stdlib.h>
#include <stdio.h>
#include <ftw.h>
#include <assert.h>
#include <argp.h>
#include <ctype.h>
#include <err.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

#define PROG_NAME "fnmon"
#define DATE_FMT "%y-%m-%dT%H:%M:%S"

#define ASSERT(msg) \
	assert (!(msg));

#define MAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })


struct entry {
	struct ts_time time;
	struct str_arr files;
};

struct args {
	char *directory;
	char *changed;
};

DECLARE_ARR (struct entry, entry);
DEFINE_ARR (struct entry, entry);

typedef int64_t i64;

DECLARE_ARR (i64, i64);
DEFINE_ARR (i64, i64);
DECLARE_ARR (i64_arr, i64_arr);
DEFINE_ARR (i64_arr, i64_arr);

struct share_files {
	char *dir;
	char *tag;
	char *tag_full_path;
	char *logfile;
	char *totals;
};

struct entry get_current_entry (char *start_dir, struct ts_time *now) {
	struct entry res = {.files = create_str_arr (8), .time = *now};
	// All the string are quoted, and the internal quotes are escaped.
	// I could backslash them. This is clearer.
	int push_quoted_paths (const char *path, const struct stat *sb, int tflag,
			struct FTW *ftwbuf) {
		struct char_arr quoted = create_char_arr (32);
		push_char_arr (&quoted, '"');
		for (const char *s = path; *s; s++) {
			if (*s == '"') {
				push_char_arr (&quoted, '\\');
			}
			push_char_arr (&quoted, *s);
		}
		push_char_arr (&quoted, '"');
		push_char_arr (&quoted, '\0');
		push_str_arr (&res.files, quoted.d);
		return 0;
	}

	// Will this work? 
	nftw (start_dir, push_quoted_paths, 0, 0) == -1
		&& (errx (1, "nftw failed"), 1);

	return res;
}

struct entry_arr get_prev_entries (char *logfile, struct ts_time *now) {
	FILE *f = fopen (logfile, "r");
	struct entry_arr res = create_entry_arr (8);
	if (!f) {
		fprintf (stderr, "no previous logfile called %s\n", logfile);
		return res;
	}
	char *line = malloc (1024);
	if (!line) ASSERT ("Malloc failed");
	size_t n = 0;
	errno = 0;

	enum token_kind {
		TOKEN_KIND_STRING,
		TOKEN_KIND_NEWLINE,
		TOKEN_KIND_WORD,
		TOKEN_KIND_ZERO,
	};
	struct token {
		char *s, *e;
		enum token_kind kind;
	};
	struct token get_token (char *p) {
		struct token res = {.s = p};
		while (isblank (*res.s)) res.s++;
		res.e = res.s + 1;
		switch (*res.s) {
			case '"':
				while (1) {
					res.kind = TOKEN_KIND_STRING;
					if (*res.e == '\0') {
						ASSERT ("Found 0 before closing \"");
					}
					if (*res.e == '"') {
						// This will break on filenames have escaped
						// backslashes? Is there such a thing?
						if (*(res.e - 1) != '\\') break;
					}
					res.e++;
				}
				res.e++;
				break;
			case '\0':
				res.kind = TOKEN_KIND_ZERO;
				break;
			case '\n':
				res.kind = TOKEN_KIND_NEWLINE;
				break;
			default:
				// Because of this there's no error-checking in this
				// function.
				while (!isblank (*res.e)) res.e++;
				res.kind = TOKEN_KIND_WORD;
				break;
		}
		return res;
	}
	struct token t = {0};
	struct entry entry;
	while (getline (&line, &n, f) != -1) {
		int n_files = 0;
		t.e = line;

		// I'm not going to bother trying to parse dates and digits, so it'll
		// just looked for TOKEN_KIND_WORD for both.
		if ((t = get_token (t.e)).kind != TOKEN_KIND_WORD)
			errx (1, "wanted an ISO date, found %.*s", (int) (t.e - t.s), t.s);
		entry.time = ts_time_from_string (t.s, t.e, &(*now).timestamp,
				&(*now).tm, TS_MATCH_ONLY_ISO);
		if (ts_errno) errx (1, "bad date: %s", ts_strerror (ts_errno));

		if ((t = get_token (t.e)).kind != TOKEN_KIND_WORD)
			errx (1, "wanted a total (digits), found %.*s", (int) (t.e - t.s),
						t.s);
		// n_files isn't used in the program. Why is it here? It's cause 
		// the file is a logfile, ie you might want to look at it.
		n_files = strtol (t.s, NULL, 10);

		entry.files = create_str_arr (8);
		while (t = get_token (t.e),
				(t.kind != TOKEN_KIND_NEWLINE && t.kind != TOKEN_KIND_ZERO)) {
			if (t.e - t.s >= PATH_MAX) errx (1, "A filename is >= PATH_MAX");
			char *filename_buf = NULL;
			asprintf (&filename_buf, "%.*s", (int) (t.e - t.s), t.s);
			push_str_arr (&entry.files, filename_buf);
		}
		assert (n_files == entry.files.n);
		push_entry_arr (&res, entry);
	}
	if (errno) errx (1, "getline failed");
	fclose (f);
	return res;

}

static void write_to_logfile (struct entry_arr entries,
		char *logfile) {
	FILE *f = fopen (logfile, "w");
	if (!f) errx (1, "%s didn't open", logfile);
	char date_buf[32];

	for (struct entry *entry = entries.d;
			entry < entries.d + entries.n; entry++) {
		ts_string_from_tm (date_buf, 32, &(*entry).time.tm, DATE_FMT);
		fprintf (f, "%s ", date_buf);
		fprintf (f, "%zu ", (*entry).files.n);
		for (char **str = (*entry).files.d;
				str < (*entry).files.d + (*entry).files.n; str++) {
			fprintf (f, "%s", *str); 
			if (str - (*entry).files.d < (*entry).files.n - 1) fputc (' ', f);
		}
		fputc ('\n', f);
	}
	fclose (f);
}


static char *get_share_dir () {
	char *home_dir = getenv ("HOME");
	if (!home_dir) errx (1, "couldn't get your HOME variable");
	char *res = NULL;
	asprintf (&res, "%s/.local/share/" PROG_NAME, home_dir);
	return res;
}

// A tag is a dir with the '/' turned into '-' and any initial '/' removed
static char *dir_to_tag (char *directory) {
	struct char_arr res = create_char_arr (strlen (directory) + 10);
	for (char *p = directory; *p; p++) {
		if (p == directory && *p == '/') continue;
		if (*p == '/') push_char_arr (&res, '-');
		// just ignores spaces.
		else if (!isspace (*p)) push_char_arr (&res, *p);
	}
	push_char_arr (&res, '\0');
	return res.d;
}

struct share_files get_share_files (char *directory) {
	struct share_files res = {0};
	res.dir = get_share_dir ();
	res.tag = dir_to_tag (directory);
	res.tag_full_path = NULL;
	asprintf (&res.tag_full_path, "%s/%s", res.dir,
			res.tag);
	asprintf (&res.logfile, "%s/logfile", res.tag_full_path);
	asprintf (&res.totals, "%s/totals", res.tag_full_path);
	return res;
}

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	struct args *args = (*state).input;
	switch (key) {
		case 'c':
			(*args).changed = arg;
			break;
		case ARGP_KEY_ARG:
			(*args).directory = arg;
			break;
		default:
			break;
	}
	return 0;
}

static struct args get_args (int argc, char **argv) {
	struct args res = {0};
	struct argp_option options[] = {
		{"changed", 'c', "TIME RANGE", 0,
			"Print files changed between these dates"},
		{0},
	};
	struct argp argp = {
		options, parse_opt, "DIRECTORY",
		"Track important files in a directory, to see if you're losing any"
	};
	argp_parse (&argp, argc, argv, 0, NULL, &res);
	if (!res.directory) errx (1, "you need to provide DIRECTORY");

	return res;
}

static void mkdir_if_not_exist (char *path, mode_t mode) {
	if (mkdir (path, mode))
		if (errno != EEXIST)
			errx (1, "couldn't make directory %s", path);
}


int main (int argc, char **argv) {
	struct args args = get_args (argc, argv);

	struct ts_time now = ts_time_from_timestamp (time (NULL));
	struct share_files share_files = get_share_files (args.directory);

	mkdir_if_not_exist (share_files.dir, 0755);
	mkdir_if_not_exist (share_files.tag_full_path, 0755);

	struct entry current_entry = get_current_entry (args.directory, &now);

	struct entry_arr entries = get_prev_entries (share_files.logfile, &now);

	push_entry_arr (&entries, current_entry);

	write_to_logfile (entries, share_files.logfile);
}
