# fnmon

"File Number Monitor". That's what it means.

The idea is is you run it periodically on directories you're paranoid
about. I keep a diary. I'm always worried I'll do something dumb and
lose an entry or thirty and not realise.

What I'll do is:

```
fnmon /home/neil/local/share/diary
```

And fnmon will count up all the files and drop it in a file in
/home/you/.local/share/fnmon/home-neil-local-share-diary/logfile.

That's a pretty long path. The name of the directory is creates is the
path to the directory you've passed with the '/' turned into '-',
intial '/'s stripped and spaces removed.

The logfile is just each entry in a line, the date first, followed by
the total number of files, and then, in quotes, all the files.

That's it. I suppose I should add in a way to check for files that are
there and then not there.
